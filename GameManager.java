public class GameManager{
	private Deck drawPile;
	private Card centerCard;
	private Card playerCard;
	
	public GameManager(){
		this.drawPile = new Deck();
		
		this.drawPile.shuffle();
		this.centerCard = this.drawPile.drawTopCard();
		
		//shouldn't i shuffle here?
		this.playerCard = this.drawPile.drawTopCard();
		
	}
	
	public String toString(){
		return  "------------------------------------------------------------- \n" +
				"Center card: " + this.centerCard + "\n" + 
				"Player card: " + this.playerCard + "\n" +
				"-------------------------------------------------------------";

	}
	
	//setter, return void
	public void dealCards(){
		//update center card
		this.centerCard = this.drawPile.drawTopCard();
		
		//update playerCard
		//we do not need a shuffle here 
		//since it will automatically pick the second card
		//in the already shuffle deck
		//since this decrements numberOfCards too
		this.playerCard = this.drawPile.drawTopCard();
		this.drawPile.shuffle();
		
	}
	
	public int getNumberOfCards(){
		return this.drawPile.length();
	}
	
	public int calculatePoints(){
		if(this.centerCard.getValue() == this.playerCard.getValue() )
			return 4;
		else if(this.centerCard.getSuit() == this.playerCard.getSuit() )
			return 2;
		else
			return -1;
	}
}