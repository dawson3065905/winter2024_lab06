public class Card{
    private String cardSuit;
    private int cardValue;

    public Card(String cardSuit, int cardValue){
        this.cardSuit = cardSuit;
        this.cardValue = cardValue;
    }

    public String getSuit(){
        return this.cardSuit;
    }

    public int getValue(){
        return this.cardValue;
    }

    public String toString(){
		String cardName = "";
		
		//assigns unique names based on value
		//don't forget to use else if next time
		//otherwise, it will automatically skip to else
		//if the first condition evaluates to false
		if(this.cardValue == 11)
			cardName = "Jack";
		else if(this.cardValue == 12)
			cardName = "Queen";
		else if(this.cardValue == 13)
			cardName = "King";
		else if(this.cardValue == 1)
			cardName = "Ace";
		//otherwise, return the numeric converted to a string
		else
			cardName = String.valueOf(cardValue);
		
        return cardName + " of " + this.cardSuit;
    }
} 