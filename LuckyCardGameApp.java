public class LuckyCardGameApp{
    public static void main(String[] args){
       GameManager manager = new GameManager();
	   int totalPoints = 0;
	   int rounds = 1;
	   
	   System.out.println("Welcome to Lucky!");
	     
		Deck geck = new Deck();
		
	   while(
	   //checks
	   //number of cards
		(manager.getNumberOfCards() > 1) &&
		//total points 
		(totalPoints < 5)
	   ){
			System.out.println("\n" + "Current rounds: " + rounds);
			
			System.out.println(manager.toString());
			totalPoints += manager.calculatePoints();
			
			manager.dealCards();
			System.out.println("Current score: " + totalPoints);
			
			rounds++;
	   }
	   
    }
	
}