import java.util.Random;

public class Deck{
    //fields
    private Card[] cards;
    private int numberOfCards;
    private Random rng;
    private String[] suits = {"clubs", "diamonds", "hearts", "spades"};
	//refactored to use ints instead, easier for comparing
    private int[] values = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};

    public Deck(){
        this.rng = new Random();
        this.cards = new Card[52];

        int i = 0;
        for(String s : this.suits){
            //suit increments after all of the possible values have been generated
            for(int v : this.values){
                //creates new card base
                this.cards[i] = new Card(s,v);

                //goes to next index
                i++;
            }

        }
        
        //logs number of cards generated
        this.numberOfCards = i;

    }

    //returns number of cards (not array length)
    public int length(){
        return this.numberOfCards;
    }

    //extracts and substitues every element from this.cards with c
    //c is then ran through toString from Card.java
    //it is then added a string which we return
    public String toString(){
        String returnString = "";

        for(Card c : cards){
            returnString += c.toString() + "\n";
        }

        return returnString;
    }

    //only takes last card in array for now
    public Card drawTopCard(){
        this.numberOfCards--;
        
		//but wait, would this not return the same card 
		//since the amount of card is getting smaller
		//should use numberOfCards instead
		return this.cards[numberOfCards];
    }

    public void shuffle(){
        Card temp;

        //uses numberOfCards instead of array length, 
		//since some position could be possiblity empty
        for(int i = 0; i < numberOfCards; i++){
            int randi = rng.nextInt(numberOfCards);

            //saves card at current index in temp
            temp = cards[i];

            //switches curent index with random index
            cards[i] = cards[randi];

            //switches current index with saved random index
            cards[randi] = temp;
        }

    }
}